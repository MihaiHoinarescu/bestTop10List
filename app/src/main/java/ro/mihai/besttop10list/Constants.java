package ro.mihai.besttop10list;

/**
 * Created by mihai on 28.03.2017.
 */

public class Constants {

    public static final String KEY_USER = "User";
    public static final String RAM_CACHE_NAME = "UserVolitileRamCache";
    public static final String DISK_CACHE_NAME = "UserVolatileDiskCache";
    public static final String PREFS_FILE = "ro.mihai.bestTop10List.SHARED_PREFERENCES";

    public static final String KEY_REMAINING_REQUESTS = "REMAINING_REQUESTS";
}