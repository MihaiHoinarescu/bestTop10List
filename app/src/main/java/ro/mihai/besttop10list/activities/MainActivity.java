package ro.mihai.besttop10list.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ro.mihai.besttop10list.Constants;
import ro.mihai.besttop10list.R;
import ro.mihai.besttop10list.adapters.UserListAdapter;
import ro.mihai.besttop10list.model.User;
import ro.mihai.besttop10list.model.UserListWrapper;
import ro.mihai.besttop10list.network.NetworkManager;
import ro.mihai.besttop10list.utils.CacheUtil;
import ro.mihai.besttop10list.utils.NetworkUtil;

public class MainActivity extends AppCompatActivity implements UserListAdapter.UserListItemClickListener {

    private static final int PERMISSION_REQUEST_CODE = 11432;

    private RecyclerView recyclerView;
    private UserListAdapter adapter;
    private Callback<UserListWrapper> listRequestCallback = new Callback<UserListWrapper>() {
        @Override
        public void onResponse(Call<UserListWrapper> call, Response<UserListWrapper> response) {
            getSharedPreferences(Constants.PREFS_FILE, MODE_PRIVATE).edit()
                    .putInt(Constants.KEY_REMAINING_REQUESTS, response.body().remaining).apply();

            if (response.isSuccessful()) {
                List<User> retrievedUsers = response.body().items;
                adapter = new UserListAdapter(MainActivity.this, retrievedUsers);
                adapter.setListener(MainActivity.this);
                recyclerView.setAdapter(adapter);
                System.out.println("MainActivity.onResponse: GETTING USERS FROM INTERNET");
                CacheUtil.getInstance().insertUsers(retrievedUsers);
            } else {
                switch (response.code()) {
                    case 404:
                        System.out.println("NetworkManager.onResponse FOR OH FOR");
                        break;
                    case 403:
                        System.out.println("NetworkManager.onResponse FORBIDDEN");
                        break;
                    case 401:
                        System.out.println("NetworkManager.onResponse IT NO UNDERSTAND");
                        break;
                    case 501:
                        System.out.println("NetworkManager.onResponse HE'S DEAD JIM");
                        break;
                    default:
                        System.out.println("NetworkManager.onResponse SOME RANDOM STUFF HAPPENED : " + response.code());
                }
            }
        }

        @Override
        public void onFailure(Call<UserListWrapper> call, Throwable t) {

            t.printStackTrace();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.top_ten_list);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        CacheUtil.init(getApplicationContext());

        checkPermissions();

    }

    private void checkPermissions() {
        List<String> permissions = new ArrayList<>();

        int state = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (state != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        state = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (state != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(this, permissions.toArray(new String[permissions.size()]), PERMISSION_REQUEST_CODE);
        } else {
            getUserList();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        boolean granted = true;

        if (requestCode == PERMISSION_REQUEST_CODE) {
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_DENIED){
                    granted = false;
                }
            }
        }

        if (granted) {
            getUserList();
        } else {
            Toast.makeText(this, "Permissions not granted, the app will now close", Toast.LENGTH_LONG);
            System.exit(0);
        }
    }

    private void getUserList() {

        List<User> cachedList = CacheUtil.getInstance().getAllUsers();
        boolean validList = true;

        for (User u : cachedList) {
            if (u == null) {
                validList = false;
                break;
            }
        }
        if (cachedList.size() > 0 && validList) {
            adapter = new UserListAdapter(MainActivity.this, cachedList);
            adapter.setListener(MainActivity.this);
            System.out.println("MainActivity.getUserList: GETTING CACHED USERS");
            recyclerView.setAdapter(adapter);
        } else {

            int remaining = getSharedPreferences(Constants.PREFS_FILE, Context.MODE_PRIVATE)
                    .getInt(Constants.KEY_REMAINING_REQUESTS, 300);

            if (remaining <= 0) {
                Toast.makeText(this, "Maximum daily requests exceeded", Toast.LENGTH_SHORT);
                return;
            }

            if (NetworkUtil.checkConnectivity(this)) {
                Map<String, String> requestOptions = new HashMap<>();
                requestOptions.put("pagesize", "10");
                requestOptions.put("order", "desc");
                requestOptions.put("sort", "reputation");
                requestOptions.put("site", "stackoverflow");

                NetworkManager.getUsers(requestOptions, listRequestCallback);
            } else {
                Toast.makeText(this, "Cannot retrieve users, please check your internet connection", Toast.LENGTH_SHORT).show();
            }
        }

    }


    @Override
    public void onItemClick(User user, View v) {
        Intent intent = new Intent(this, UserDetailsActivity.class);
        intent.putExtra(Constants.KEY_USER, user);
        startActivity(intent);

    }
}
