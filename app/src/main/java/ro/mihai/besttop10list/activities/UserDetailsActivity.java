package ro.mihai.besttop10list.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import ro.mihai.besttop10list.Constants;
import ro.mihai.besttop10list.R;
import ro.mihai.besttop10list.model.User;

/**
 * Created by mihai on 28.03.2017.
 */

public class UserDetailsActivity extends AppCompatActivity {

    private User currentUser;
    private TextView nameText, locationText, goldText, silverText, bronzeText;
    private ImageView userProfileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        nameText = (TextView)findViewById(R.id.user_name);
        locationText = (TextView)findViewById(R.id.user_locatiom);
        goldText = (TextView)findViewById(R.id.user_gold);
        silverText = (TextView)findViewById(R.id.user_silver);
        bronzeText = (TextView)findViewById(R.id.user_bronze);
        userProfileImage = (ImageView)findViewById(R.id.user_image);

        currentUser = getIntent().getParcelableExtra(Constants.KEY_USER);

        if (currentUser != null) {
            nameText.setText(currentUser.getName());
            locationText.setText(currentUser.getLocation());
            goldText.setText(String.valueOf(currentUser.getBadges().getGold()));
            silverText.setText(String.valueOf(currentUser.getBadges().getSilver()));
            bronzeText.setText(String.valueOf(currentUser.getBadges().getBronze()));
            if (currentUser.getProfilePictureURL() != null) {
                Picasso.with(this).load(currentUser.getProfilePictureURL()).into(userProfileImage);
            }

        }

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        currentUser = savedInstanceState.getParcelable(Constants.KEY_USER);

        if(currentUser != null) {
            nameText.setText(currentUser.getName());
            locationText.setText(currentUser.getLocation());
            goldText.setText(String.valueOf(currentUser.getBadges().getGold()));
            silverText.setText(String.valueOf(currentUser.getBadges().getSilver()));
            bronzeText.setText(String.valueOf(currentUser.getBadges().getBronze()));
            if (currentUser.getProfilePictureURL() != null) {
                Picasso.with(this).load(currentUser.getProfilePictureURL()).into(userProfileImage);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(Constants.KEY_USER, currentUser);
        super.onSaveInstanceState(outState);
    }
}
