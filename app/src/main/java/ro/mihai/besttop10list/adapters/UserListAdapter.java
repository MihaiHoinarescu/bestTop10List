package ro.mihai.besttop10list.adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import ro.mihai.besttop10list.R;
import ro.mihai.besttop10list.model.User;

/**
 *
 * Created by mihai on 27.03.2017.
 */

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {

    private UserListItemClickListener listener;
    private List<User> userList;
    private Context context;

    public UserListAdapter(Context context, List<User> list) {
        userList = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = userList.get(position);
        holder.name.setText(user.getName());
        if (user.getProfilePictureURL() != null) {
            Picasso.with(context).load(user.getProfilePictureURL()).into(holder.profileImage);
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public void setListener(UserListItemClickListener listener) {
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        ImageView profileImage;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            name = (TextView)itemView.findViewById(R.id.name);
            profileImage = (ImageView)itemView.findViewById(R.id.image);

        }

        @Override
        public void onClick(View v) {
            User selectedUser = userList.get(getAdapterPosition());

            if(selectedUser != null)
            listener.onItemClick(selectedUser, v);
        }
    }

    public interface UserListItemClickListener{

        void onItemClick(User user, View v);
    }
}
