package ro.mihai.besttop10list.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import ro.mihai.besttop10list.utils.BitmapUtil;

/**
 * Created by mihai on 27.03.2017.
 */

public class User implements Parcelable {

    @SerializedName("display_name")
    private String name;
    @SerializedName("profile_image")
    private String profilePictureURL;
    @SerializedName("badge_counts")
    private BadgeCounts badges;
    private String location;

    protected User(Parcel in) {
        name = in.readString();
        profilePictureURL = in.readString();
        badges = in.readParcelable(BadgeCounts.class.getClassLoader());
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePictureURL() {
        return profilePictureURL;
    }

    public void setProfilePictureURL(String profilePictureURL) {
        this.profilePictureURL = profilePictureURL;
    }

    public BadgeCounts getBadges() {
        return badges;
    }

    public void setBadges(BadgeCounts badges) {
        this.badges = badges;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(profilePictureURL);
        dest.writeParcelable(badges, flags);
        dest.writeString(location);
    }
}
