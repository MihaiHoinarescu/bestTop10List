package ro.mihai.besttop10list.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mihai on 28.03.2017.
 */

public class UserListWrapper {
    public List<User> items;

    @SerializedName("quota_remaining")
    public int remaining;

}
