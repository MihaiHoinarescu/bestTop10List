package ro.mihai.besttop10list.network;

import android.support.annotation.Nullable;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ro.mihai.besttop10list.model.UserListWrapper;

/**
 * Created by mihai on 27.03.2017.
 */

public class NetworkManager {

    private static int REMAINING_REQUESTS = 300;
    private static final String DEFAULT_FILTER = "!40DEKwKLVP9geExqw";

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.stackexchange.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private static NetworkRequests REQUESTS = retrofit.create(NetworkRequests.class);

    public static void getUsers(Map<String, String> options, Callback<UserListWrapper> callback) {
        if (!options.containsKey("filter") && options.get("filter") == null) {
            options.put("filter", DEFAULT_FILTER);
        }
        Call<UserListWrapper> request = REQUESTS.getUsers(options);

        request.enqueue(callback);
//        request.enqueue(new Callback<UserListWrapper>() {
//            @Override
//            public void onResponse(Call<UserListWrapper> call, Response<UserListWrapper> response) {
//                if (response.isSuccessful()) {
//                    if (response.body().remaining != REMAINING_REQUESTS) {
//                        REMAINING_REQUESTS = response.body().remaining;
//                    }
//
//                } else {
//                    switch (response.code()) {
//                        case 404:
//                            System.out.println("NetworkManager.onResponse FOR OH FOR");
//                            break;
//                        case 403:
//                            System.out.println("NetworkManager.onResponse FORBIDDEN");
//                            break;
//                        case 401:
//                            System.out.println("NetworkManager.onResponse IT NO UNDERSTAND");
//                            break;
//                        case 501:
//                            System.out.println("NetworkManager.onResponse IT'S DEAD CAPT'N");
//                            break;
//                        default:
//                            System.out.println("NetworkManager.onResponse SOME RANDOM STUFF HAPPENED : " + response.code());
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<UserListWrapper> call, Throwable t) {
//                t.printStackTrace();
//            }
//        });
    }
}

