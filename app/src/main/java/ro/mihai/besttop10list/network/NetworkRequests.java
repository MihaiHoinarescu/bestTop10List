package ro.mihai.besttop10list.network;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import ro.mihai.besttop10list.model.UserListWrapper;

/**
 * Created by mihai on 27.03.2017.
 */

public interface NetworkRequests {

    @GET("/2.2/users")
    Call<UserListWrapper> getUsers(@QueryMap Map<String, String> options);
}
