package ro.mihai.besttop10list.utils;

import android.content.Context;
import android.os.Environment;

import com.google.gson.Gson;
import com.iagocanalejas.dualcache.Builder;
import com.iagocanalejas.dualcache.DualCache;
import com.iagocanalejas.dualcache.interfaces.Cache;
import com.iagocanalejas.dualcache.interfaces.Serializer;
import com.iagocanalejas.dualcache.interfaces.SizeOf;
import com.iagocanalejas.gsonserializer.GsonSerializer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ro.mihai.besttop10list.BuildConfig;
import ro.mihai.besttop10list.Constants;
import ro.mihai.besttop10list.model.User;

/**
 * Created by mihai on 28.03.2017.
 */

public class CacheUtil {

    private static final Gson gson = new Gson();
    private static CacheUtil instance;
    private static Context context;
    private static final File cacheFile = new File(Environment.getExternalStorageDirectory(), "cache.json");

    private DualCache<String, User> userCache = new Builder<String, User>(Constants.RAM_CACHE_NAME, BuildConfig.VERSION_CODE)
            .enableLog()
            .noDisk()
            .useReferenceInRam(100, new SizeOf<User>() {
                @Override
                public int sizeOf(User user) {
                    return 1;
                }
            }).useVolatileCache(60)
            .build();

    private DualCache<String, User> userDiskCache;

    private CacheUtil() {
        File testFile = new File(context.getExternalCacheDir(), "cache.json");
        userDiskCache = new Builder<String, User>(Constants.DISK_CACHE_NAME, BuildConfig.VERSION_CODE)
                .enableLog()
                .noRam()
                .useSerializerInDisk(1024000, testFile, new Serializer<User>() {
                    @Override
                    public User fromString(String s) {
                        return gson.fromJson(s, User.class);
                    }

                    @Override
                    public String toString(User user) {
                        return gson.toJson(user);
                    }
                })
                .useVolatileCache(180)
                .build();
    }

    public static CacheUtil getInstance() throws IllegalStateException {
        if (context == null) {
            throw new IllegalStateException("You must call CacheUtil.init(Context ctx) before using it!");
        }
        if (instance == null) {
            instance = new CacheUtil();
        }
        return instance;
    }

    public static void init(Context ctx) {
        context = ctx;
    }

    public void insertUsers(List<User> users) {
        for (int i = 0; i < users.size(); i++) {
            userCache.put(String.valueOf(i), users.get(i));
            userDiskCache.put(String.valueOf(i), users.get(i));
        }
    }

    public List<User> getAllUsers() {
        int i = 0;
        boolean validList = true;
        List<User> result = new ArrayList<>();
        while (userCache.contains(String.valueOf(i))) {
            result.add(userCache.get(String.valueOf(i)));
            i++;
        }

        for (User u : result) {
            if (u == null) {
                validList = false;
            }
        }

        if (validList && result.size() > 0) {
            return result;
        } else {
            i = 0;
            result.clear();

            while (userDiskCache.contains(String.valueOf(i))) {
                result.add(userDiskCache.get(String.valueOf(i)));
                i++;
            }
        }

        return result;

    }

}